package com.tiger.listazakupow;

/**
  Created by Tygrysek on 3/22/2016.
 **/
public interface AnimationEssentials
{
    int delay = 300;
    int lightblue = 0xFF7ec0ee;
    int lightgrey = 0xFF606060;
    int lightpink = 0xFFffb6c1;
}
